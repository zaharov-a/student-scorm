<?php

// exit("\r\n<pre>\r\n".__FILE__.':'.__LINE__."\r\n" . print_r(array($_SERVER), true) . "\r\n</pre>\r\n");
$file = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'];
if (is_file($file)) {
    $type = mime_content_type($file);
    if ($type == 'text/x-asm') {
        $type = 'text/css';
    } else {
        if ('js' == substr($file, -2)) {
            $type = 'application/javascript';
        } else {
            if ('css' == substr($file, -3)) {
                $type = 'text/css';
            }
        }
    }
    header("Content-type: {$type}");
    readfile($file);
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        html, body {
            height: 100%;
        }
    </style>
</head>
<body>
<?php

include __DIR__ . '/../vendor/autoload.php';
$menu = new \Ox3a\Scorm\Model\Menu\MenuModel(
    (new \Ox3a\Scorm\Model\XmlParser())->parseFile(__DIR__ . '/demo/unpack/imsmanifest.xml')
);
/**
 * @param \Ox3a\Scorm\Model\Menu\ItemModel[] $items
 */
function showMenu($items)
{
    if (!$items) {
        return;
    }
    ?>
    <ul>
        <?php foreach ($items as $item) { ?>
            <li>
                <a <?php echo $item->href ? "href=\"/demo/unpack/{$item->href}\"" : '' ?>
                        id="<?php echo $item->identifier ?>" target="a">
                    <?php echo $item->title ?>
                </a>
                <?php showMenu($item->getChildren()) ?>
            </li>
        <?php } ?>
    </ul>
    <?php

}

?>
<div style="display: flex; height: 100%">
    <div>
        <?php showMenu($menu->getItems()); ?>
    </div>
    <div style="flex: 1">
        <iframe name="a" height="100%" width="100%"></iframe>

    </div>
</div>
<script src="api.js"></script>
</body>
</html>
