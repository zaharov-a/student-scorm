// https://scorm.com/scorm-explained/technical-scorm/run-time/
//https://scorm.com/scorm-explained/technical-scorm/run-time/run-time-reference/#section-2
var Api = /** @class */ (function () {
    function Api() {
        this._values = {
            // 'cmi.suspend_data': '{}',
            'cmi.core.student_name': 'Иванов Иван Иванович',
        };
    }
    Api.prototype.LMSInitialize = function (value) {
        console.log('LMSInitialize', arguments);
        return true;
    };
    Api.prototype.LMSFinish = function (value) {
        console.log('LMSFinish', arguments);
        return false;
    };
    Api.prototype.LMSGetValue = function (element) {
        console.info('LMSGetValue', element, this._values[element]);
        return this._values[element] || '';
    };
    Api.prototype.LMSSetValue = function (element, value) {
        console.error('LMSSetValue', element, value);
        this._values[element] = value;
        return true;
    };
    Api.prototype.LMSCommit = function (value) {
        console.log('LMSCommit', value);
        return true;
    };
    Api.prototype.LMSGetLastError = function () {
        console.log('LMSGetLastError');
        return '000';
    };
    Api.prototype.LMSGetErrorString = function (errorCode) {
        console.log('LMSGetErrorString', errorCode);
        return '';
    };
    Api.prototype.LMSGetDiagnostic = function (errorCode) {
        console.log('LMSGetDiagnostic', errorCode);
        return 'true';
    };
    return Api;
}());
var API = new Api();
