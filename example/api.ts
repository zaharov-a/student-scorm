type CMIElement = string;

type CMIErrorCode = string;

// https://scorm.com/scorm-explained/technical-scorm/run-time/
//https://scorm.com/scorm-explained/technical-scorm/run-time/run-time-reference/#section-2
class Api
{
    protected _values = {
        // 'cmi.suspend_data': '{}',
        'cmi.core.student_name': 'Иванов Иван Иванович',
    };


    LMSInitialize(value: string): boolean
    {
        console.log('LMSInitialize', arguments);
        return true;
    }


    LMSFinish(value: string): boolean
    {
        console.log('LMSFinish', arguments);

        return false;
    }


    LMSGetValue(element: CMIElement): string
    {
        console.info('LMSGetValue', element, this._values[element]);
        return this._values[element] || '';
    }


    LMSSetValue(element: CMIElement, value: string): boolean
    {
        console.error('LMSSetValue', element, value);
        this._values[element] = value;
        return true;
    }


    LMSCommit(value: string): boolean
    {
        console.log('LMSCommit', value);
        return true;
    }


    LMSGetLastError(): CMIErrorCode
    {
        console.log('LMSGetLastError');
        return '000';
    }


    LMSGetErrorString(errorCode: CMIErrorCode): string
    {
        console.log('LMSGetErrorString', errorCode);
        return '';
    }


    LMSGetDiagnostic(errorCode: CMIErrorCode): string
    {
        console.log('LMSGetDiagnostic', errorCode);
        return 'true';
    }
}

var API = new Api();
