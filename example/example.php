<?php

include __DIR__ . '/../vendor/autoload.php';

$parser     = new \Ox3a\Scorm\Model\XmlParser();
$unArchiver = new \Ox3a\Scorm\Model\UnArchiver();

$file = $argv[1];

if (!is_file($file)) {
    throw new RuntimeException("Файл не найден: {$file}");
}

if (true !== $unArchiver->open($file)) {
    throw new RuntimeException('');
}

$unArchiver->extractTo(__DIR__ . '/demo/unpack');

$unArchiver->close();

$scorm = $parser->parseFile(__DIR__ . '/demo/unpack/imsmanifest.xml');

$menu = new \Ox3a\Scorm\Model\Menu\MenuModel($scorm);

exit("\r\n<pre>\r\n" . __FILE__ . ':' . __LINE__ . "\r\n" . print_r([$menu], true) . "\r\n</pre>\r\n");


exit("\r\n<pre>\r\n" . __FILE__ . ':' . __LINE__ . "\r\n" . print_r([$scorm->getOrganizations()->getOrganization($scorm->getOrganizations()->default)], true) . "\r\n</pre>\r\n");
