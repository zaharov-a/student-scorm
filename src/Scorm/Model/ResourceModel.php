<?php


namespace Ox3a\Scorm\Model;


/**
 * Class ResourceModel
 * @package Ox3a\Scorm\Model
 * @property string $resourcetype
 * @property string $identifier
 * @property string $base
 * @property string $href
 */
class ResourceModel extends AbstractModel
{

    protected $_properties = [
        'resourcetype' => null,
        'identifier'   => null,
        'base'         => null,
        'href'         => null,
    ];

    /**
     * @var FileModel[]
     */
    protected $_files = [];

    protected $_dependencies = [];


    /**
     * @return FileModel[]
     */
    public function getFiles()
    {
        return $this->_files;
    }


    /**
     * @return array
     */
    public function getDependencies()
    {
        return $this->_dependencies;
    }


    /**
     * @param FileModel $file
     * @return $this
     */
    public function addFile($file)
    {
        $this->_files[] = $file;

        return $this;
    }

}
