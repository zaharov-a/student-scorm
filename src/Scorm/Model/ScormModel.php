<?php

namespace Ox3a\Scorm\Model;

/**
 * Class ScormModel
 * @package Ox3a\Scorm\Model
 * @property string $identifier
 * @property string $version
 */
class ScormModel extends AbstractModel
{
    protected $_properties = [
        'identifier' => null,
        'version'    => null,
    ];

    /**
     * @var ResourcesModel
     */
    protected $_resources;

    /**
     * @var OrganizationsModel
     */
    protected $_organizations;


    /**
     * @return ResourcesModel
     */
    public function getResources()
    {
        return $this->_resources;
    }


    /**
     * @param ResourcesModel $resources
     * @return ScormModel
     */
    public function setResources($resources)
    {
        $this->_resources = $resources;
        return $this;
    }


    /**
     * @return OrganizationsModel
     */
    public function getOrganizations()
    {
        return $this->_organizations;
    }


    /**
     * @param OrganizationsModel $organizations
     * @return ScormModel
     */
    public function setOrganizations($organizations)
    {
        $this->_organizations = $organizations;
        return $this;
    }


}
