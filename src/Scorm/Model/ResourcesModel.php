<?php


namespace Ox3a\Scorm\Model;


class ResourcesModel extends AbstractModel
{

    /**
     * @var ResourceModel[]
     */
    protected $_children = [];


    /**
     * @return ResourceModel[]
     */
    public function getChildren()
    {
        return $this->_children;
    }


    /**
     * @param ResourceModel $resource
     * @return $this
     */
    public function addResource(ResourceModel $resource)
    {
        $this->_children[$resource->identifier] = $resource;

        return $this;
    }


    /**
     * @param $identifier
     * @return ResourceModel|null
     */
    public function getResource($identifier)
    {
        return isset($this->_children[$identifier]) ? $this->_children[$identifier] : null;
    }

}
