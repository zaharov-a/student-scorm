<?php


namespace Ox3a\Scorm\Model;


/**
 * Class OrganizationModel
 * @package Ox3a\Scorm\Model
 * @property string $title
 * @property string $identifier
 */
class OrganizationModel extends AbstractModel
{
    protected $_properties = [
        'title'      => null,
        'identifier' => null,
    ];


    protected $_items = [];


    /**
     * @return array
     */
    public function getItems()
    {
        return $this->_items;
    }


    /**
     * @param ItemModel $item
     * @return $this
     */
    public function addItem($item)
    {
        $this->_items[$item->identifier] = $item;

        return $this;
    }


    public function getItem($identifier)
    {
        return isset($this->_items[$identifier]) ? $this->_items[$identifier] : null;
    }
}
