<?php


namespace Ox3a\Scorm\Model;

use RuntimeException;
use ZipArchive;

/**
 * Разархиватор учебника
 * Class UnArchiver
 * @package Ox3a\Scorm\Model
 */
class UnArchiver
{
    const ERROR_NOT_FOUND = 1;

    /**
     * Объект учебника
     * @var ScormModel
     */
    protected $_scorm;

    /**
     * @var ZipArchive
     */
    protected $_archive;


    /**
     * Открыть архив
     * @param string $filePath
     * @return bool|int
     * @throws RuntimeException
     */
    public function open($filePath)
    {
        if (!is_file($filePath)) {
            throw new RuntimeException('Нет файла');
        }

        $archive = new ZipArchive();

        if (true !== ($status = $archive->open($filePath))) {
            return $status * 10;
        }

        if (!($manifest = $archive->getFromName('imsmanifest.xml'))) {
            return self::ERROR_NOT_FOUND;
        }

        $parser = new XmlParser();

        $this->_scorm   = $parser->parse($manifest);
        $this->_archive = $archive;

        return true;
    }


    /**
     * Закрыть архив
     */
    public function close()
    {
        if ($this->_archive) {
            $this->_archive->close();
            $this->_archive = null;
            $this->_scorm   = null;
        }
    }


    /**
     * Выгрузить архив в папку
     * @param $dirPath
     * @throws RuntimeException
     */
    public function extractTo($dirPath)
    {
        if (!is_writable($dirPath)) {
            throw new RuntimeException('Нет прав на создание файлов');
        }

        file_put_contents($dirPath . '/imsmanifest.xml', $this->_archive->getFromName('imsmanifest.xml'));

        $fileList = $this->getFileList();

        foreach ($fileList as $file) {
            $dir = dirname($file);
            if ($dir != '.' && !file_exists($dirPath . '/' . $dir)) {
                mkdir($dirPath . '/' . $dir, 0777, true);
            }
            // возможно файл упакован с полным путем
            if (!($stat = $this->_archive->statName($file))) {
                if (!($stat = $this->_archive->statName('/' . $file))) {
                    throw new RuntimeException(sprintf("Файл не найден: %s", $file));
                }
            }

            file_put_contents($dirPath . '/' . $file, $this->_archive->getFromIndex($stat['index']));
        }
    }


    /**
     * Получить модель учебника
     * @return ScormModel
     */
    public function getScorm()
    {
        return $this->_scorm;
    }


    /**
     * Получить список файловых ресурсов
     * @return string[]
     */
    public function getFileList()
    {
        if (!$this->_scorm) {
            return [];
        }

        $list = [];

        foreach ($this->_scorm->getResources()->getChildren() as $resource) {
            if ($resource->getFiles()) {
                foreach ($resource->getFiles() as $file) {
                    $list[] = $file->href;
                }
            }
        }

        return $list;
    }


}
