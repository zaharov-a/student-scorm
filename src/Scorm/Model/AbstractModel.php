<?php


namespace Ox3a\Scorm\Model;


abstract class AbstractModel
{

    protected $_properties = [];


    public function __get($name)
    {
        return $this->__isset($name) ? $this->_properties[$name] : null;
    }


    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_properties)) {
            $this->_properties[$name] = $value;
        }
    }


    public function __isset($name)
    {
        return isset($this->_properties[$name]);
    }
}
