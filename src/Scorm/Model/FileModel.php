<?php


namespace Ox3a\Scorm\Model;

/**
 * Class FileModel
 * @package Ox3a\Scorm\Model
 * @property string $href
 */
class FileModel extends AbstractModel
{

    protected $_properties = [
        'href' => null,
    ];


}
