<?php


namespace Ox3a\Scorm\Model\Menu;


use Ox3a\Scorm\Model\AbstractModel;

/**
 * Class ItemModel
 * @package Ox3a\Scorm\Model\Menu
 * @property string $title
 * @property string $href
 * @property string $identifier
 */
class ItemModel extends AbstractModel
{
    protected $_properties = [
        'title'      => null,
        'href'       => null,
        'identifier' => null,
    ];


    /**
     * @var ItemModel[]
     */
    protected $_children = [];


    public function addItem($item)
    {
        $this->_children[] = $item;
    }


    /**
     * @return ItemModel[]
     */
    public function getChildren()
    {
        return $this->_children;
    }


}
