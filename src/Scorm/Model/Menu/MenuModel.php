<?php


namespace Ox3a\Scorm\Model\Menu;


use Ox3a\Scorm\Model\ItemModel as ScormItemModel;
use Ox3a\Scorm\Model\ScormModel;

/**
 * Меню по учебнику
 * Class MenuModel
 * @package Ox3a\Scorm\Model\Menu
 */
class MenuModel
{

    /**
     * @var ItemModel[]
     */
    protected $_items = [];

    /**
     * @var ScormModel
     */
    protected $_scorm;


    public function __construct(ScormModel $scorm)
    {
        $this->_scorm = $scorm;
        foreach ($scorm->getOrganizations()->getChildren() as $organization) {
            $item             = new ItemModel();
            $item->title      = $organization->title;
            $item->identifier = $organization->identifier;

            foreach ($organization->getItems() as $scormItem) {
                $item->addItem($this->getItem($scormItem));
            }

            $this->_items[] = $item;
        }
    }


    private function getItem(ScormItemModel $scormItem)
    {
        $resources        = $this->_scorm->getResources();
        $item             = new ItemModel();
        $item->title      = $scormItem->title;
        $item->identifier = $scormItem->identifier;

        if ($scormItem->identifierref) {
            $resource   = $resources->getResource($scormItem->identifierref);
            $item->href = $resource->href;
        }
        foreach ($scormItem->getChildren() as $child) {
            $item->addItem($this->getItem($child));
        }

        return $item;
    }


    /**
     * @return ItemModel[]
     */
    public function getItems()
    {
        return $this->_items;
    }


}
