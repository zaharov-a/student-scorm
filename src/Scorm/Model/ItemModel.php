<?php


namespace Ox3a\Scorm\Model;


/**
 * Class ItemModel
 * @package Ox3a\Scorm\Model
 * @property string  $title
 * @property string  $identifier
 * @property string  $identifierref
 * @property boolean $isvisible
 * @property mixed   $parameters
 */
class ItemModel extends AbstractModel
{

    protected $_properties = [
        'title'         => null,
        'identifier'    => null,
        'identifierref' => null,
        'isvisible'     => false,
        'parameters'    => null,
    ];

    protected $_title;

    /**
     * @var ItemModel[]
     */
    protected $_children = [];


    /**
     * @return ItemModel[]
     */
    public function getChildren()
    {
        return $this->_children;
    }


    /**
     * @param ItemModel $item
     * @return $this
     */
    public function addItem($item)
    {
        $this->_children[] = $item;
        return $this;
    }
}
