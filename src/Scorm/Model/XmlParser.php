<?php


namespace Ox3a\Scorm\Model;


use RuntimeException;
use SimpleXMLElement;

/**
 * Class XmlParser
 * https://www.imsproject.org/sites/default/files/xsd/imscp_rootv1p1p2.xsd
 * @package Ox3a\Scorm\Model
 */
class XmlParser
{
    public function parseFile($filePath)
    {
        if (!is_file($filePath)) {
            throw new RuntimeException('Нет файла');
        }
        return $this->parse(file_get_contents($filePath));
    }


    /**
     * @param string $content
     * @return ScormModel
     */
    public function parse($content)
    {
        $scorm = new ScormModel();

        $reader = simplexml_load_string($content);

        if (!$reader) {
            throw new RuntimeException('Не смог распознать манифест');
        }

        $scorm->identifier = (string)$reader->attributes()->identifier;
        $scorm->version    = (string)$reader->attributes()->version;

        $scorm
            ->setOrganizations($this->getOrganizations($reader->organizations))
            ->setResources($this->getResources($reader->resources));

        return $scorm;

    }


    /**
     * @param SimpleXMLElement $reader
     * @return OrganizationsModel
     */
    public function getOrganizations(SimpleXMLElement $reader)
    {

        $organizations = new OrganizationsModel();

        $organizations->default = (string)$reader->attributes()->default;

        foreach ($reader->children() as $organization) {
            $organizations->addOrganization($this->getOrganization($organization));
        }

        return $organizations;
    }


    public function getOrganization(SimpleXMLElement $reader)
    {
        $organization = new OrganizationModel();

        $organization->identifier = ((string)$reader->attributes()->identifier);
        $organization->title      = ((string)$reader->title);

        foreach ($reader->item as $item) {
            $organization->addItem($this->getItem($item));
        }

        return $organization;
    }


    public function getItem(SimpleXMLElement $reader)
    {
        $item = new ItemModel();

        $item->title         = (string)$reader->title;
        $item->identifier    = (string)$reader->attributes()->identifier;
        $item->identifierref = (string)$reader->attributes()->identifierref;
        $item->isvisible     = (boolean)$reader->attributes()->isvisible;

        foreach ($reader->item as $child) {
            $item->addItem($this->getItem($child));
        }

        return $item;
    }


    /**
     * @param SimpleXMLElement $reader
     * @return ResourcesModel
     */
    public function getResources(SimpleXMLElement $reader)
    {
        $resources = new ResourcesModel();

        foreach ($reader->resource as $resource) {
            $resources->addResource($this->getResource($resource));
        }

        return $resources;
    }


    public function getResource(SimpleXMLElement $reader)
    {
        $resource = new ResourceModel();

        $resource->identifier   = ((string)$reader->attributes()->identifier);
        $resource->base         = ((string)$reader->attributes()->base);
        $resource->href         = ((string)$reader->attributes()->href);
        $resource->resourcetype = ((string)$reader->attributes()->resourcetype);

        foreach ($reader->file as $file) {
            $resource->addFile($this->getFile($file));
        }

        return $resource;
    }


    /**
     * @param SimpleXMLElement $reader
     * @return FileModel
     */
    public function getFile(SimpleXMLElement $reader)
    {
        $file = new FileModel();

        $file->href = (string)$reader->attributes()->href;

        return $file;
    }
}
