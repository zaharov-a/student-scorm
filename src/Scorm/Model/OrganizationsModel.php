<?php


namespace Ox3a\Scorm\Model;


/**
 * Class OrganizationsModel
 * @package Ox3a\Scorm\Model
 * @property string $default
 */
class OrganizationsModel extends AbstractModel
{

    protected $_properties = [
        'default' => null,
    ];


    /**
     * @var OrganizationModel[]
     */
    protected $_children = [];


    /**
     * @return OrganizationModel[]
     */
    public function getChildren()
    {
        return $this->_children;
    }


    /**
     * @param OrganizationModel $item
     * @return $this
     */
    public function addOrganization($item)
    {
        $this->_children[$item->identifier] = $item;
        return $this;
    }


    /**
     * @param string $identifier
     * @return OrganizationModel|null
     */
    public function getOrganization($identifier)
    {
        return isset($this->_children[$identifier]) ? $this->_children[$identifier] : null;
    }
}
